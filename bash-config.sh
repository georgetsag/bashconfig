#Macros for quick navigation#
alias ll='ls -alFh'
alias la='ls -A'
alias l='ls -CF'
alias ..='cd ..'

#Other macros#
alias h='history'
alias apt='sudo apt'
alias apt-get='sudo apt-get'
alias top='htop'
alias space='du -d 1 -h .'
alias grrep='grep -r'

proc() {
	ps aux | grep $1
}

#Export custom prompt style
export PS1="\[\033[38;5;21m\]\[\033[48;5;42m\]\A\[$(tput sgr0)\]\[\033[38;5;4m\]\u\[$(tput sgr0)\]\[\033[38;5;14m\]@\[$(tput sgr0)\]\[\033[38;5;36m\]\h\[$(tput sgr0)\]\[\033[38;5;59m\]:\[$(tput sgr0)\]\[\033[38;5;130m\]:\[$(tput sgr0)\]\[\033[38;5;196m\]\w\[$(tput sgr0)\] \[$(tput sgr0)\]\[\033[38;5;11m\]\\$\[$(tput sgr0)\]"
#Array of apt packages
my_pkgs=( "gcc" "vim" "geany" "git" "net-tools" "transmission" "libreoffice" "gradle" "maven" "htop")

install-pkgs() {

	sudo apt update
	sudo apt-get update
	sudo apt upgrade
	sudo apt-get upgrade

	for i in "${my_pkgs[@]}"
	do
		sudo apt-get install $i -y
	done
}

install-java() {
	for i in "default-jdk" "default-jre" "default-jdk-doc"
	do
		sudo apt-get install $i -y
	done
}

install-metalang() {
	for i in "flex" "flex++" "bison" "bison++"
	do
		sudo apt-get install $i -y
	done
}

     echo -e '
     _/^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\_
    /   #   O   #OOOO   #   #    OOO    #   #   #        %%%     %%%%   \
    |   #  O    #       #   #   #   #   #   #   #       %   %   %       |
    |   # O     #       #O  #   #   #   #   #   #       %   %   %       |
    |   #O      #OO     # O #   #   #   #   #   #  (\033[33;5m*\033[0m)  %   %    %%%    |
    |   # O     #       #  O#   #   #    # #    #       %   %       %   |
    |   #  O    #       #   #   #   #    # #    #       %   %       %   /
    |   #   O   #OOOO   #   #    OOO      #     #        %%%    %%%%   /
    \                                                                 /
     ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        Welcome
        '

